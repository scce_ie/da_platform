package info.scce.dime.app.demo;

import java.io.FileInputStream;
import java.io.IOException;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;


import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Random;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileInputStream;
import org.rosuda.REngine.Rserve.RFileOutputStream;
import org.rosuda.REngine.Rserve.RserveException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

import de.ls5.dywa.generated.util.DomainFileController;
import de.ls5.dywa.generated.util.FileReference;

public class RScripts {
	public static String xzzz = "";

	/**
	 * @return The current system time.
	 */
	public static Date getCurrentTime() {
		return new Date();
	}

	public static String createRandomWord(int len) {
		String name = "";
		for (int i = 0; i < len; i++) {
			int v = 1 + (int) (Math.random() * 26);
			char c = (char) (v + 'a' - 1);
			name += c;
		}
		return name;
	}

	public static void search_dir_docker_java_iterative(File dir) {
		try {

			for (File file : dir.listFiles()) {
				if (file.isDirectory()) {
					if (file.getCanonicalPath().toLowerCase().contains("asset")) {
						xzzz = file.getCanonicalPath().toString().toLowerCase().split("asset")[0] + "asset";
						break;
					} else {
						search_dir_docker_java_iterative(file);
					}
				}

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void files_dir_docker_java_iterative(File dir) {
		try {

			for (File file : dir.listFiles()) {
				if (file.isDirectory()) {
					files_dir_docker_java_iterative(file);
				} else {
					// if (file.getCanonicalPath().toLowerCase().contains("chart1"))
					{
						xzzz += "     file:" + file.getAbsolutePath();
					}
				}

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void files_dir_docker_java_current(File dir) {
		try {
			String[] files = dir.list();
			for (String file : files) {
				xzzz += " " + file;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static String getFourrow(String fileName) throws IOException,
	 * CsvException { // final de.ls5.dywa.generated.util.FileReference delegate //
	 * String a = (String) delegate.fileName; // System.out.println(delegate); //
	 * System.out.println(" ------------- "+delegate.toString()+" ------------- ");
	 * 
	 * // String fileName= "la-riots-deaths.csv";
	 * 
	 * ClassLoader classloader = Thread.currentThread().getContextClassLoader();
	 * InputStream is = null; BufferedReader br = null; String returnValue = null;
	 * String line = null; int count = 0; // String row[] = null;
	 * System.out.println("--------File name" + fileName); try { is =
	 * classloader.getResourceAsStream(fileName); br = new BufferedReader(new
	 * InputStreamReader(is)); while (count < 4) {
	 * 
	 * line = br.readLine(); returnValue = line + returnValue + "\n"; count++; } }
	 * catch (Exception e) { return returnValue; } finally { br.close(); }
	 * 
	 * return returnValue; }
	 */
	public static String connection_mongoDB(String url,String database_name,String collection_name) {
		String file_handler = "";

		try {
			String command="connection_to_mongodb('"+collection_name+"','"+ database_name+"','"+ url+"')";
			System.out.println(command);
			file_handler= Rcon.getInstance().getConnection()
					.parseAndEval( command).asString().toString();
			

		} catch (Exception exx) {
			System.out.println("Errrrrrrrrrrrrrrrrrrr1:"+exx.toString());
return exx.toString();
		}
		return file_handler;
	}
	public static String split_data_frame(String file_handler,String col1,String col2,String col3,String col4) {
		String df="filtered_dfs";
				try {
					String command=df+"=dataframe_split('"+file_handler+"','"+col1+"','"+ col2+"','"+ col3+"','"+ col4+"')";
					System.out.println(command);
					 Rcon.getInstance().getConnection()
							.parseAndEval( command);
					

				} catch (Exception exx) {
					System.out.println("Errrrrrrrrrrrrrrrrrrr1:"+exx.toString());
		return exx.toString();
				}
				return df;
			}
	
	public static String read_CSV(String file_name) {
		String RandomS = createRandomWord(4);
		String server_file = RandomS + ".csv";
		String file_handler = "dataR";
		try {
			transfer_toserver(Rcon.getInstance().getConnection(), file_name, server_file);
			Rcon.getInstance().getConnection()
					.parseAndEval(file_handler + "<- read.csv('" + server_file + "', stringsAsFactors = FALSE)");
		} catch (Exception exx) {

		}
		return file_handler;

	}
	
	
	public static FileReference plot_quad_summary_charts(String dataframe, String x1, String y1)
	// CsvException
	{
		System.out.println("quad line chart function functionnnnnnnnnnnnnn");
		return quad_summary_charts(Rcon.getInstance().getConnection(), dataframe, x1,y1);
	}
	public static FileReference quad_summary_charts(RConnection c, String dataframe, String x1, String y1) {
		System.out.println("plot startedddddddddddd 193");
		String RandomS = createRandomWord(4);
		String resultant_file = RandomS + "_plot4.jpg";
		try {
			
			c.parseAndEval("jpeg('temp1.jpg')");
			String command="plot_quad_summary_charts("+dataframe+",'"+x1+"','"+ y1+"')";
			System.out.println(command);
			c.parseAndEval(command);
			c.parseAndEval("dev.off()");
			System.out.println("plot startedddddddddddd 185");
		} catch (Exception ex) {
			System.out.println("Summary endedddd with exception");
		}
		return transfer_toclient(c, resultant_file, "temp1.jpg");
	}
	public static FileReference plot_dual_linechart(String dataframe, String x1, String y1, String title1, String x2, String y2, String title2) // throws IOException,
	// CsvException
	{
		System.out.println("dual line chart function functionnnnnnnnnnnnnn");
		return dual_line_chart(Rcon.getInstance().getConnection(), dataframe, x1,y1,title1,x2,y2,title2);
	}
	
	public static FileReference dual_line_chart(RConnection c, String dataframe, String x1, String y1, String t1, String x2, String y2, String t2) {
		System.out.println("plot startedddddddddddd 185");
		String RandomS = createRandomWord(4);
		String resultant_file = RandomS + "_plot4.jpg";
		try {
			
			c.parseAndEval("jpeg('temp1.jpg')");
			String command="plot_dual_line_chart("+dataframe+",'"+x1+"','"+y1+"','"+ t1+"','"+ x2+"','"+ y2+"','"+ t2+"')";
			c.parseAndEval(command);
			c.parseAndEval("dev.off()");
			System.out.println("plot startedddddddddddd 185");
		} catch (Exception ex) {
			System.out.println("Summary endedddd with exception");
		}
		return transfer_toclient(c, resultant_file, "temp1.jpg");
	}
	public static FileReference R_summary(RConnection c, String file_handler, String col_name) {
		System.out.println("Summary startedddddddddddd");
		String RandomS = createRandomWord(4);
		String resultant_file = RandomS + "_plot4.jpg";
		try {
			if(!(file_handler.isEmpty() || file_handler==null))
			{
				col_name = file_handler + "$" + col_name;
			}
			c.parseAndEval("jpeg('temp1.jpg')");
			c.parseAndEval("d<-summary(" + col_name + ")");
			c.parseAndEval("grid.newpage()");
			c.parseAndEval("grid.table(t(d))");
			c.parseAndEval("dev.off()");
			System.out.println("Summary endedddd");
		} catch (Exception ex) {
			System.out.println("Summary endedddd with exception");
		}
		return transfer_toclient(c, resultant_file, "temp1.jpg");
	}

	public static FileReference R_histogram(RConnection c, String file_handler, String col_name, String breaks,
			String title, String x_label, String y_label, String color) {
		System.out.println("histrogram started");
		String RandomS = createRandomWord(4);
		String resultant_file = RandomS + "_plot4.jpg";
		try {
			if(!(file_handler.isEmpty() || file_handler==null))
			{
				col_name = file_handler + "$" + col_name;
			}
			// String server_file = RandomS + ".csv";
			// transfer_toserver(c, file_path, server_file);
			c.parseAndEval("jpeg('temp.jpg')");
			c.parseAndEval("hist(" + col_name + ",breaks=" + breaks + ", main='" + title + "',xlab='" + x_label
					+ "',ylab='" + y_label + "',col='" + color + "')");
			c.parseAndEval("dev.off()");

			// transfer_toclient(c, new
			// File(System.getProperty("user.dir")).toString()+"/temp.jpg","temp.jpg") ;
			// xx= transfer_toclient(c, xzzz+"/temp.jpg","temp.jpg") ;
			// return transfer_toclient(c, xzzz + "/" + resultant_file, "temp.jpg");
			System.out.println("histrogram end");

		} catch (Exception ex) {
			System.out.println("histrogram end with exception");

			// return ex.toString();
		}
		return transfer_toclient(c, resultant_file, "temp.jpg");

		// return xx;
	}

	public static FileReference R_wordcloud(RConnection c, String fileName, String col_name, String min_frequency,
			String max_words) {
		System.out.println("word cloud started");
		String RandomS = createRandomWord(4);
		String resultant_file = RandomS + "_plot.jpg";
		try {

			col_name = "dataD$" + col_name;
			String server_file = RandomS + ".csv";
			transfer_toserver(c, fileName, server_file);

			c.parseAndEval("dataD <- read.csv('" + server_file + "', stringsAsFactors = FALSE)");
			c.parseAndEval("text <- " + col_name);
			c.parseAndEval("jpeg('temp.jpg')");
			c.parseAndEval(
					"docs <- Corpus(VectorSource(text)) ; dtm <- TermDocumentMatrix(docs) ; matrix <- as.matrix(dtm) ; words <- sort(rowSums(matrix),decreasing=TRUE) ; df <- data.frame(word = names(words),freq=words)");
			c.parseAndEval("wordcloud(words = df$word, freq = df$freq, min.freq = " + min_frequency + ",max.words="
					+ max_words + ", random.order=FALSE, rot.per=0.35, colors=brewer.pal(8, 'Dark2'))");
			c.parseAndEval("dev.off()");
			System.out.println("word cloud end");
			// atransfer_toclient(c, resultant_file,"temp.jpg") ;
		} catch (Exception ex) {
			System.out.println("word cloud end with exception");
		}

		return transfer_toclient(c, resultant_file, "temp.jpg");
	}

	public static FileReference plot_R_wordcloud(String fileName, String col_name, String min_frequency,
			String max_words) // throws IOException, CsvException
	{
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		// xy = R_wordcloud(c,fileName,"surname","1","200");
		// REXP x = c.eval("R.version.string");
		// c.close();
		// return xx;

		return R_wordcloud(Rcon.getInstance().getConnection(), fileName, col_name, min_frequency, max_words);

	}

	public static FileReference plot_R_histogram(String file_handler, String col_name, String breaks, String title,
			String x_label, String y_label, String color) // throws IOException, CsvException
	{
		System.out.println("plot R histrogrammmmmmmmmmmmmmmmm");
		// xy = R_wordcloud(c,fileName,"surname","1","200");
		// REXP x = c.eval("R.version.string");
		// c.close();
		// return xx;

		return R_histogram(Rcon.getInstance().getConnection(), file_handler, col_name, breaks, title, x_label, y_label,
				color);
	}

	public static FileReference generate_summary(String file_handler, String col_name) // throws IOException,
																						// CsvException
	{
		System.out.println("Summery functionnnnnnnnnnnnnn");
		// xy = R_wordcloud(c,fileName,"surname","1","200");
		// REXP x = c.eval("R.version.string");
		// c.close();
		// return xx;

		return R_summary(Rcon.getInstance().getConnection(), file_handler, col_name);
	}

	public static String executeRcommand(String command) {
		try {
			REXP x = Rcon.getInstance().getConnection().eval(command);// eval("R.version.string");
			return x.asString().toString();
		} catch (Exception exx) {
			return "Error in execution";
		}
	}

	public static double mean(RConnection c, String col_name) {
		System.out.println("inside mean function");
		/*System.out.println(list.size());

		double[] myvalues = new double[list.size()];
		int i = 0;
		for (Double d : list) {
			myvalues[i] = (d);
			i++;
		}*/
		try {
		//	c.assign("myvalues", myvalues);
			REXP x1 = c.eval("mean("+col_name+")");
			System.out.println(x1.asDouble());
			return x1.asDouble();
		} catch (Exception xx) {
			System.out.println("inside mean function exception" + xx.toString());

			return 0.0;
		}
	}

	public static double calculate_mean(String col_name) {
		// System.out.println(mean(Rcon.getInstance().getConnection(),myvalues));
		return mean(Rcon.getInstance().getConnection(), col_name);
		// return 0.5;
	}
	public static String MoveDoubleColToRserver( List <Double>  list) {
		return DoubleColToRSERVER (Rcon.getInstance().getConnection(), list);
	}
	public static String DoubleColToRSERVER(RConnection c, List <Double>  list) {
		String RandomS = createRandomWord(4);
		System.out.println("sizeeeeeeeeeeeeeeee"+list.size());
		double[] myvalues = new double[list.size()];
		int i=0;
		for (Double d : list) {
	    	myvalues[i]=(d);
	    	i++;
		}
			try
			{
			 c.assign(RandomS, myvalues);
		//	REXP x = Rcon.getInstance().getConnection().eval("mean(myvalues)");// eval("R.version.string");
			return RandomS;
		} catch (Exception exx) {
			return "Error in execution";
		}
	}
	/*
	 * public static String R2(String r1) {
	 * 
	 * try { //example 0 //Quickchart_plot(r1); RConnection c = new
	 * RConnection("172.17.0.5",6311); /////////////////////////////////////////
	 * 
	 * //example 1 simple R string REXP x = c.eval("R.version.string"); c.close();
	 * 
	 * return "ex1 "+x.asString().toString();
	 * 
	 * /////////////////////////////////////////////////// //example 2 // usage of
	 * existing functions with inputs from java double[] myvalues = {1.0, 1.5, 2.2,
	 * 0.5, 0.9, 1.12}; c.assign("myvalues", myvalues); REXP x1 =
	 * c.eval("mean(myvalues)"); c.close(); //return
	 * "ex 2 Mean "+Double.toString(x1.asDouble());
	 * 
	 * //example 3 String vector = "c(1,2,3,4,5,6,7,8,9)"; c.eval("meanVal=mean(" +
	 * vector + ")"); double mean = c.eval("meanVal").asDouble(); c.close();
	 * //return "ex 3 Mean "+ Double.toString(mean); //example 4 // to get the
	 * dimensions of dataset in total rows and columns. int [] dim=
	 * c.eval("dim(iris)").asIntegers(); c.close(); //return
	 * ("ex 4 ROWs: "+Integer.toString( dim[0]) +" - COLs: "+ Integer.toString(
	 * dim[1])); //example 5
	 * 
	 * c.eval("x <- c(21, 62, 10, 53)");
	 * c.eval("labels <- c('London', 'New York', 'Singapore', 'Mumbai')");
	 * c.eval("png(file = 'city2.png')");
	 * c.parseAndEval("print(pie(x,labels));dev.off()"); REXP xp =
	 * c.parseAndEval("r=readBin('city2.png','raw',1024*1024)");
	 * c.parseAndEval("unlink('city2.png'); r"); byte[] bytes=xp.asBytes(); Path
	 * path = Paths.get("ac2.png"); c.close(); Files.write(path,bytes);;
	 * 
	 * 
	 * // return path.ge() + path.getParent().toString()+ path.toUri().toString();
	 * //example 6 //to get summary of variables/ //formatting is very bad, need to
	 * handle accorting to the widget where is it going to be displayed //String
	 * summaryy=c.eval("paste(capture.output(print(summary(iris))),collapse='\\n')")
	 * .asString(); //c.close(); //return "ex 6 - "+summaryy; //example 7 //getting
	 * array of result from R e.g. double in this case double[] dd=
	 * c.eval("rnorm(20000)").asDoubles(); for (int i=0;i<dd.length;i++) {
	 * xzzz+=Double.toString(dd[i]); //System.out.println(dd.); } return
	 * "ex7 xzzz= " +xzzz ;
	 * 
	 * 
	 * //example 8 //getting 2D data from R //String m = c.
	 * eval("paste(capture.output(print(matrix(sample(0:1,100, rep=T),ncol=10))),collapse='\\n')"
	 * ).asString(); //return m; //double [][] xx =
	 * c.eval("matrix(sample(0:1,100, rep=T),ncol=10)").asDoubleMatrix(); //return
	 * Double.toString(xx[1][9]);
	 * 
	 * //example 9
	 * 
	 * 
	 * //imp block //File folder = new File(System.getProperty("user.dir")); //File
	 * folder = new File(r1); //files_dir_docker_java_current(folder);
	 * //files_dir_docker_java_iterative(folder);
	 * 
	 * //return "ex 5 - count = "+Integer.toString(count) +" -"+xzzz;
	 * 
	 * try(FileReader reader = new FileReader("c.R")) {
	 * c.parseAndEval(reader.toString()); //c.voidEval(reader.toString());
	 * //c.eval(reader.toString()); } double[] myvalues = {1.0, 1.5, 2.2, 0.5, 0.9,
	 * 1.12}; c.assign("myvalues", myvalues); REXP x1 =
	 * c.eval("customMean25(myvalues)"); c.close(); return
	 * "ex 2 Mean "+Double.toString(x1.asDouble()); //return
	 * ("ex 4 ROWs: "+Integer.toString( dim[0]) +" - COLs: "+ Integer.toString(
	 * dim[1]));
	 * 
	 * // return ""; } catch (Exception eec) { return "exception " + eec.toString();
	 * }
	 * 
	 * 
	 * }
	 */
	/*
	 * public static String getFourrowTwo(String a) throws IOException, CsvException
	 * { // String a = delegate.getFileName(); ClassLoader classloader =
	 * Thread.currentThread().getContextClassLoader(); InputStream is = null;
	 * BufferedReader br = null; String returnValue = null; String line = null; int
	 * count = 0; // String row[] = null; System.out.println("--------File name" +
	 * a); try { is = classloader.getResourceAsStream(a); br = new
	 * BufferedReader(new InputStreamReader(is)); while (count < 4) { line =
	 * br.readLine(); returnValue = line + returnValue + "\n"; count++; } } catch
	 * (Exception e) { return returnValue; } finally { br.close(); } return
	 * returnValue; }
	 */
	public static void transfer_toserver(RConnection r, String client_file, String server_file) {
		byte[] b = new byte[8192000];
		try {
			/* the file on the client machine we read from */
			// BufferedInputStream client_stream = new BufferedInputStream(
			// new FileInputStream( new File( client_file ) ) );
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			/* the file on the client machine we read from */
			BufferedInputStream client_stream = new BufferedInputStream(classloader.getResourceAsStream(client_file));
			/* the file on the server we write to */
			RFileOutputStream server_stream = r.createFile(server_file);
			/* typical java IO stuff */
			int c = client_stream.read(b);
			while (c >= 0) {
				server_stream.write(b, 0, c);
				c = client_stream.read(b);
			}
			server_stream.close();
			client_stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static FileReference transfer_toclient(RConnection r, String client_file, String server_file) {
		byte[] b = new byte[8192000];
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			/* the file on the client machine we write to */
			BufferedOutputStream client_stream = new BufferedOutputStream(new FileOutputStream(new File(client_file)));

			/* the file on the server machine we read from */
			RFileInputStream server_stream = r.openFile(server_file);
			/* typical java io stuff */
			int c = server_stream.read(b);
			while (c >= 0) {
				client_stream.write(b, 0, c);
				baos.write(b, 0, c);
				c = server_stream.read(b);
			}
//		      int cc="dsad".toByteArray();
			client_stream.close();
			server_stream.close();
			baos.close();
			InputStream stream = new ByteArrayInputStream(baos.toByteArray());

			// return exportToFile("oic");
			return getDomainFileController().storeFile(client_file, stream);
		} catch (Exception emn) {
		}

		return exportToFile("Error");
		// return getDomainFileController().storeFile("entries1.csv", stream);
	}

	private static DomainFileController getDomainFileController() {
		final BeanManager bm = CDI.current().getBeanManager();
		final Bean<DomainFileController> bean = (Bean<DomainFileController>) bm
				.resolve(bm.getBeans(DomainFileController.class));
		final CreationalContext<DomainFileController> cctx = bm.createCreationalContext(bean);
		final DomainFileController fileController = (DomainFileController) bm.getReference(bean, bean.getBeanClass(),
				cctx);
		return fileController;
	}

	public static FileReference exportToFile(String xx) {
		// InputStream stream = new ByteArrayInputStream("ahmad awais
		// chaudhary".getBytes(StandardCharsets.UTF_8));
		// InputStream stream = new FileInputStream( file_name_with_extension);
		// return getDomainFileController().storeFile(file_name_with_extension, stream);
		InputStream stream = new ByteArrayInputStream(exportToText().getBytes(StandardCharsets.UTF_8));
		return getDomainFileController().storeFile(xx + "entry.csv", stream);
	}

	public static String exportToText() {

		StringBuffer buffer = new StringBuffer();
		buffer.append("There is some error, somewhere");
		return buffer.toString();
	}
}
