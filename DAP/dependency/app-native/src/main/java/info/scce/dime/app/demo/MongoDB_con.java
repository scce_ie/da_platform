package info.scce.dime.app.demo;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

public class MongoDB_con {

	private static MongoDB_con instance;
	private static MongoClient connection;

	private MongoDB_con() {
		try {
			ConnectionString connectionString = new ConnectionString(
					"mongodb+srv://pygeul:pygeul123@cluster0.do0sg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
			MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(connectionString)
					.build();
			MongoDB_con.connection = MongoClients.create(settings);
			// MongoDBCon.connection = new RConnection("localhost", 6311);
			// this.connection = new RConnection("172.17.0.7", 6311);
		} catch (Exception ex) {
			System.out.println("exception in connection to MongoDB, check connection string");
		}
	}

	public MongoClient getConnection() {
		// System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzz11111111111");
		return connection;
	}

	public static MongoDB_con getInstance() {
		// System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzz2222222222222222222222");
		if (instance == null) {
			instance = new MongoDB_con();
		}
		return instance;
	}

}