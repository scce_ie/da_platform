package info.scce.dime.app.demo;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Device {
	public String id;
	public String name;
	public String adminState;
	public String operatingState;
	public List<String> labels;
	public List<Command> commands;
	public String location;
	public JSONObject jsonRepresentation;

	public String getURL() {
		Command command = commands.get(0);
		return command.getURL();
	}

	public String getValue() {
		jsonRepresentation = new JSONObject(RESTApi.read_URL_to_Json(this.getURL()));
		return getReadings().getJSONObject(0).getString("value");
	}

	public JSONArray getReadings() {
		return jsonRepresentation.getJSONArray("readings");
	}

	public List<String> getLabels() {
		return labels;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public String toString() {
		return name;
	}
}
