package info.scce.dime.app.demo;

public class Command {
	public String created;
	public String modified;
	public String id;
	public String name;
	public GETOperation get;
	public SETOperation set;
	public PUTOperation put;

	public String toString() {
		return name;
	}

	public String getURL() {
//		return get.url.replace("edgex-core-command", "localhost");
		return get.url.replace("edgex-core-command", "host.docker.internal");

	}
}
