package info.scce.dime.app.demo;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;

public class RESTApi {

	public static String rest_read_str_str(String url, String input_var, String input, String output) {
		System.out.println("hereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
		String customURL = url + "/?" + input_var + "=" + input;
		System.out.println(customURL);
		JSONObject obj = new JSONObject("{\"country_name\":\"United Kingdom\"}");
		try {
			if (obj.getString(output) != null) {
				return obj.getString(output);
			} else {
				return "No Result";
			}
		} catch (Exception e) {
			return "No Result";
		}
	}

	public static List<String> rest_read_str_list(String url, String input_var, String input, String output) {
		String customURL = url + "/?" + input_var + "=" + input;
		System.out.println(customURL);
		JSONObject obj = new JSONObject(
				"{\"country_name\":[\"Pakistan\",\"United States\",\"United Kingdom\",\"Oman\",\"Ireland\"]}");
		try {
			if (!obj.isEmpty()) {
				List<String> tempResult = new ArrayList<>();

				for (int i = 0; i < obj.getJSONArray(output).length(); i++) {
					tempResult.add(obj.getJSONArray(output).getString(i));
				}
				System.out.println(tempResult);
				return tempResult;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static String read_URL_to_Json(String customURL) {
		String inlineReceivedJson = "";
		try {
			System.out.println("inside rest api");

			// server connection and getting response
			// System.out.println("aa");
			URL url = new URL(customURL);
			// System.out.println("bb");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.connect();
			// System.out.println("cc");
			// Getting the response code
			int responsecode = conn.getResponseCode();
//System.out.println(responsecode);
//System.out.println("dd");
			if (responsecode != 200) {
				// System.out.println("ee");
				System.out.println("Error1:inside rest api");
				throw new RuntimeException("HttpResponseCode: " + responsecode);
			} else {
				System.out.println("Scanner:inside rest api");
				Scanner scanner = new Scanner(url.openStream());
				// Write all the JSON data into a string using a scanner
				while (scanner.hasNext()) {
					inlineReceivedJson += scanner.nextLine();
				}
				// System.out.println("gg");
				// Close the scanner
				scanner.close();
				conn.disconnect();
				System.out.println("Finished:inside rest api");

				// System.out.println("hh");
				// System.out.println(inlineReceivedJson);
				// by now complete json is written in inlineReceivedJson variable
			}
		} catch (Exception e) {
			System.out.println("Error2:inside rest api");

			e.printStackTrace();

		}
		return inlineReceivedJson;
	}

}
