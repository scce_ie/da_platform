package info.scce.dime.app.demo;

import java.util.List;

public class Response {
	public String code;
	public String description;
	public List<String> expectedValues;

	public String toString() {
		return code + " " + description;
	}
}
