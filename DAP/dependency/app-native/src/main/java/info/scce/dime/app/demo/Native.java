package info.scce.dime.app.demo;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Collection of static methods for native SIBs
 */
public class Native {
	public static ArrayList<Device> deviceList = new ArrayList<Device>();
	// public static void main (String [ ] args) {
	public static List<String> read_device_data(String json, String device_name) {
		//String [] resultantString=null; 
		List mylist = new ArrayList<String>();

		ObjectMapper objMapper = new ObjectMapper();
		try {
			//List<EdgeX_DB> myObjects = objMapper.readValue(json, new TypeReference<List<EdgeX_DB>>(){});
			EdgeX_DB[] myObjects = objMapper.readValue(json, EdgeX_DB[].class);
			
			for (EdgeX_DB edgeX_DB : myObjects) {
	            //if (Long.parseLong(edgeX_DB.created)>1655824515000L)//must be commented
	            {
				if(edgeX_DB.name.equals(device_name))
				{
					
					mylist.add(objMapper.writeValueAsString(edgeX_DB).toString());
					//ObjectMapper objMr = new ObjectMapper();

				   // resultantString[i]="kk";//objMr.writeValueAsString(edgeX_DB).toString();
				//i++;
				}}
			}
		//	resultantString=new String[mylist.size()];
			//for (int i = 0; i < mylist.size(); i++) {
				//resultantString[i] = mylist.get(i);
	       // }
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mylist;
	}
	public static String read_device(String json, String device_name) {
		Native.generateDevices(json);
		try {
			Device dev = Native.getDeviceInformation(device_name);
			try {
				String sb = dev.getValue();
				return sb;
			} catch (Exception exx) {
				return "Error in parsing";
			}
		} catch (Exception ee) {
			return "Device not found in the API";
		}
	}

	/*
	 * private static String getDevices() { return
	 * urlToJson("http://host.docker.internal:48082/api/v1/device"); //return
	 * urlToJson("http://localhost:48082/api/v1/device"); }
	 */
	public static Device getDeviceInformation(String name) {
		if (deviceList != null) {
			List<Device> devList = deviceList.stream().filter(d -> d.name.equals(name)).collect(Collectors.toList());

			if (devList.isEmpty()) {
				throw new DeviceNotFoundException("Device not found in the API");
			} else {
				return devList.get(0);
			}
		}
		return null;
	}

	private static void generateDevices(String json) {
		ObjectMapper objMapper = new ObjectMapper();
		try {
			deviceList = objMapper.readValue(json, new TypeReference<ArrayList<Device>>() {
			});
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public static Device filterByName(String name) {
		if (deviceList != null) {
			// System.out.println("ii");
			return deviceList.stream().filter(d -> d.name.equals(name)).collect(Collectors.toList()).get(0);
		}
		return null;
	}

}