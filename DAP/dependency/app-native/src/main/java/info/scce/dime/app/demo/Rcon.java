package info.scce.dime.app.demo;

import org.rosuda.REngine.Rserve.RConnection;

public class Rcon {
	private static Rcon instance;
	private static RConnection connection;

	private Rcon() {
		// System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyy");
		try {
			Rcon.connection = new RConnection("host.docker.internal", 6311);
			// this.connection = new RConnection("172.17.0.7", 6311);
		} catch (Exception ex) {
			// System.out.println("eyyyyyyyyyyyyyyyyyyyyyyyyyyy");
		}
	}

	public RConnection getConnection() {
		return connection;
	}

	public static Rcon getInstance() {
		if (instance == null) {
			instance = new Rcon();
		}
		return instance;
	}

}
