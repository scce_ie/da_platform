package info.scce.dime.app.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCursor;

import static com.mongodb.client.model.Filters.*;

public class MongoDB_Scripts {
	public static void sortOfMainFunc() {
		System.out.println("byeeeeeeeeeeeeeeeeeeeeee");
		System.out.println();
		System.out.println(insertMultiple(MongoDB_con.getInstance().getConnection(), "confirm", "edgexfoundry",
				Arrays.asList(new Document().append("device", "Short Circuit 3").append("value", 105),
						new Document().append("device", "Short Circuit 4").append("value", 406))));

		System.out.println("byeeeeeeeeeeeeeeeeeeeeee");

		// List <Document>
		// list=selectAll(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry");
		List<Document> list = selectConditional(MongoDB_con.getInstance().getConnection(), "confirm", "edgexfoundry",
				eq("device", "test2"));

	}

	public static List<Double> fetchAll(String db_name, String collection_name, String column) {
		return extractDoubleColumn(selectAll(MongoDB_con.getInstance().getConnection(), db_name, collection_name),
				column);
		// List
		// list=selectConditional(MongoDBCon.getInstance().getConnection(),"confirm","edgexfoundry",eq("device","test2"));

		// return
		// insertSingle(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",new
		// Document().append("device", "test11").append("value", 100));
	}

	public static List<Double> fetchConditional(String db_name, String collection_name, String column, String filter) {
		return extractDoubleColumn(selectConditional(MongoDB_con.getInstance().getConnection(), db_name,
				collection_name, eq("device", filter)), column);
		// List
		// list=selectConditional(MongoDBCon.getInstance().getConnection(),"confirm","edgexfoundry",eq("device","test2"));

		// return
		// insertSingle(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",new
		// Document().append("device", "test11").append("value", 100));
	}

	public static String insertSingleDeviceOneCol(String db_name, String collection_name, String device_name,
			String device_value) {
		return insertSingle(MongoDB_con.getInstance().getConnection(), db_name, collection_name,
				new Document().append("device", device_name).append("value", device_value.toString()));
		// return
		// insertSingle(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",new
		// Document().append("device", "test11").append("value", 100));
	}
	public static String insertDeviceData(String db_name, String collection_name, String device_data) {
		
		
		ObjectMapper objMapper = new ObjectMapper();
		try {
			
			EdgeX_DB mObj1 = objMapper.readValue(device_data, EdgeX_DB.class);
			Date date = new Date(Long.parseLong(mObj1.created));				   
	        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");  
	        String strDate = dateFormat.format(date);
			return insertSingle(MongoDB_con.getInstance().getConnection(), db_name, collection_name,
					new Document()
					.append("time", strDate)
					.append("device", mObj1.device)
					.append("name", mObj1.name)				
					.append("value", Double.valueOf(mObj1.value).longValue())
					);
		
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			return "Records insertion failed";
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			return "Records insertion failed";
		}
		
		

		// return
		// insertSingle(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",new
		// Document().append("device", "test11").append("value", 100));
	}
	public static String insertMultiDeviceOneCol(String db_name, String collection_name, String device_name,
			String device_value) {
		List<Document> listM = new ArrayList();
		String[] temp_result = device_value.replace("[", "").replace("]", "").split(",");
		for (String res : temp_result) {
			listM.add(new Document().append("device", device_name).append("value", res));
		}
		// System.out.println(insertMultiple(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",listM));
		return insertMultiple(MongoDB_con.getInstance().getConnection(), db_name, collection_name, listM);
		// return
		// insertSingle(MongoDB_con.getInstance().getConnection(),"confirm","edgexfoundry",new
		// Document().append("device", "test11").append("value", 100));
	}

	public static String[] extractStringColumn(List<Document> list, String column) {
		String[] myvalues = new String[list.size()];
		int i = 0;
		for (Document d : list) {
			myvalues[i] = d.get(column).toString();
			i++;
		}
		return myvalues;
	}

	public static int[] extractIntegerColumn(List<Document> list, String column) {
		int[] myvalues = new int[list.size()];
		int i = 0;
		for (Document d : list) {
			myvalues[i] = Integer.parseInt(d.get(column).toString());
			i++;
		}
		return myvalues;
	}

	public static List<Double> extractDoubleColumn(List<Document> list, String column) {
		System.out.println("inside extract doule col func");
		/*
		 * double[] myvalues = new double[list.size()]; int i = 0; for (Document d :
		 * list) { myvalues[i] = Double.parseDouble(d.get(column).toString()); i++; }
		 * return myvalues;
		 */
		List<Double> col = new ArrayList();
		for (Document d : list) {
			col.add(Double.parseDouble((String) d.get("value").toString()));
		}
		return col;
	}

	public static List<Document> selectAll(MongoClient mc, String database, String collection) {
		System.out.println("inside select all");
		List<Document> list = new ArrayList();
		MongoCursor<Document> menuCursor = mc.getDatabase(database).getCollection(collection).find().iterator();
		while (menuCursor.hasNext()) {
			// System.out.println(menuCursor.next().get("value"));
			list.add(menuCursor.next());
			// list.add(5.5);
		}
		return list;
	}

	public static List<Document> selectConditional(MongoClient mc, String database, String collection, Bson query) {

		List<Document> list = new ArrayList();
		MongoCursor<Document> menuCursor = mc.getDatabase(database).getCollection(collection).find(query).iterator();
		while (menuCursor.hasNext()) {
			list.add(menuCursor.next());
		}
		return list;
	}
	public static String insertSingle(MongoClient mc, String database, String collection, Document document) {
		try {
			mc.getDatabase(database).getCollection(collection).insertOne(document);
			return "Record inserted sucessfully";
		} catch (Exception xx) {
			return "Record insertion failed";
		}
	}

	public static String insertMultiple(MongoClient mc, String database, String collection, List documents) {
		try {
			mc.getDatabase(database).getCollection(collection).insertMany(documents);
			return "Records inserted sucessfully";
		} catch (Exception xx) {
			return "Records insertion failed";
		}
	}
}
