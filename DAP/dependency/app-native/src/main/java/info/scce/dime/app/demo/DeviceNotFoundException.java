package info.scce.dime.app.demo;

public class DeviceNotFoundException extends RuntimeException {

	public DeviceNotFoundException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
