***Enviournment must be built and deployed after every major addition in R scripts.***
**How to build:**

docker build -t rserve Rserve/ --no-cache

**How to run:**

docker run --name EnvR -p 6311:6311 --rm rserve:latest

**Following step is only needed if Rserver is not accessible via localhost**
**How to inspect containers and images to get R enviournment IP**

docker container ls

docker network ls

docker network inspect bridge

*get ip of image EnvR and use it in DIME Configuraitons*

